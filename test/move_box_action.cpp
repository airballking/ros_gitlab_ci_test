#include <gtest/gtest.h>
#include <ros/ros.h>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include <actionlib/client/simple_action_client.h>
#pragma GCC diagnostic pop
#include <actionlib/client/terminal_state.h>
#include <box_robot_definition/MoveBoxAction.h>

TEST(TESTSuite, sendGoal)
{
  // Create the action client
  actionlib::SimpleActionClient<box_robot_definition::MoveBoxAction> ac("move_box", true);
  EXPECT_TRUE(ac.waitForServer(ros::Duration(4)));

  // Send a goal to the action
  box_robot_definition::MoveBoxGoal goal;
  goal.z_angle = M_PI;;

  ac.sendGoal(goal);
  EXPECT_TRUE(ac.waitForResult(ros::Duration(4)));
  EXPECT_EQ(ac.getState(), actionlib::SimpleClientGoalState::SUCCEEDED);
}

int main(int argc,
         char **argv)
{
  ros::init(argc, argv, "test_move_box_action");
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
